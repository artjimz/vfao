﻿using Microsoft.AspNetCore.Mvc;
using Weber.VFAO.Web.Controllers;

namespace Weber.VFAO.Web.Public.Controllers
{
    public class AboutController : VFAOControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}