using Microsoft.AspNetCore.Mvc;
using Weber.VFAO.Web.Controllers;

namespace Weber.VFAO.Web.Public.Controllers
{
    public class HomeController : VFAOControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}