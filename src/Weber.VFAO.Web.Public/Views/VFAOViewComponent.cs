﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Weber.VFAO.Web.Public.Views
{
    public abstract class VFAOViewComponent : AbpViewComponent
    {
        protected VFAOViewComponent()
        {
            LocalizationSourceName = VFAOConsts.LocalizationSourceName;
        }
    }
}