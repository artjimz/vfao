﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace Weber.VFAO.Web.Public.Views
{
    public abstract class VFAORazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected VFAORazorPage()
        {
            LocalizationSourceName = VFAOConsts.LocalizationSourceName;
        }
    }
}
