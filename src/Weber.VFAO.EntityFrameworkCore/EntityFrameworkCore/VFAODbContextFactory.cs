﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Weber.VFAO.Configuration;
using Weber.VFAO.Web;

namespace Weber.VFAO.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class VFAODbContextFactory : IDbContextFactory<VFAODbContext>
    {
        public VFAODbContext Create(DbContextFactoryOptions options)
        {
            var builder = new DbContextOptionsBuilder<VFAODbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            VFAODbContextConfigurer.Configure(builder, configuration.GetConnectionString(VFAOConsts.ConnectionStringName));
            
            return new VFAODbContext(builder.Options);
        }
    }
}