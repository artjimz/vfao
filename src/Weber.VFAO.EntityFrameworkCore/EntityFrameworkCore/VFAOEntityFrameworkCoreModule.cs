﻿using Abp.EntityFrameworkCore.Configuration;
using Abp.IdentityServer4;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;
using Weber.VFAO.Migrations.Seed;

namespace Weber.VFAO.EntityFrameworkCore
{
    /// <summary>
    /// Entity framework Core module of the application.
    /// </summary>
    [DependsOn(
        typeof(AbpZeroCoreEntityFrameworkCoreModule), 
        typeof(VFAOCoreModule), 
        typeof(AbpZeroCoreIdentityServerEntityFrameworkCoreModule)
        )]
    public class VFAOEntityFrameworkCoreModule : AbpModule
    {
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<VFAODbContext>(configuration =>
                {
                    VFAODbContextConfigurer.Configure(configuration.DbContextOptions, configuration.ConnectionString);
                });
            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(VFAOEntityFrameworkCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            if (!SkipDbSeed)
            {
                SeedHelper.SeedHostDb(IocManager);
            }
        }
    }
}
