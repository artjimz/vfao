using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Weber.VFAO.EntityFrameworkCore
{
    public static class VFAODbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<VFAODbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }
    }
}