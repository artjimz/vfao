﻿using System.Collections.Generic;
using Weber.VFAO.Auditing.Dto;
using Weber.VFAO.Dto;

namespace Weber.VFAO.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
