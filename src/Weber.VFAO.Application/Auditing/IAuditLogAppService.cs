using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Weber.VFAO.Auditing.Dto;
using Weber.VFAO.Dto;

namespace Weber.VFAO.Auditing
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<PagedResultDto<AuditLogListDto>> GetAuditLogs(GetAuditLogsInput input);

        Task<FileDto> GetAuditLogsToExcel(GetAuditLogsInput input);
    }
}