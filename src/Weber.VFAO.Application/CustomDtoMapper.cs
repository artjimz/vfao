using Abp.Application.Features;
using Abp.UI.Inputs;
using AutoMapper;
using Weber.VFAO.Authorization.Users;
using Weber.VFAO.Authorization.Users.Dto;
using Weber.VFAO.Editions.Dto;

namespace Weber.VFAO
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());

	        configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
	        configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
	        configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
	        configuration.CreateMap<IInputType, FeatureInputTypeDto>()
				.Include<CheckboxInputType, FeatureInputTypeDto>()
		        .Include<SingleLineStringInputType, FeatureInputTypeDto>()
		        .Include<ComboboxInputType, FeatureInputTypeDto>();

	        configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
	        configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
		        .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();

	        configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
	        configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
		        .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

			/* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */
		}
    }
}