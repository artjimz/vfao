﻿using Abp.Application.Services;
using Weber.VFAO.Dto;
using Weber.VFAO.Logging.Dto;

namespace Weber.VFAO.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
