﻿using System.Collections.Generic;

namespace Weber.VFAO.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
