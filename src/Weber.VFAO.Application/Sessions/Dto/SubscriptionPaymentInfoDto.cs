﻿using Abp.AutoMapper;
using Weber.VFAO.MultiTenancy.Payments;

namespace Weber.VFAO.Sessions.Dto
{
    [AutoMapFrom(typeof(SubscriptionPayment))]
    public class SubscriptionPaymentInfoDto
    {
        public decimal Amount { get; set; }
    }
}