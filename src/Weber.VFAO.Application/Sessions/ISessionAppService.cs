﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Weber.VFAO.Sessions.Dto;

namespace Weber.VFAO.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
