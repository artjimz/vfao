﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Weber.VFAO.MultiTenancy.HostDashboard.Dto;

namespace Weber.VFAO.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}