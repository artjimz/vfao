﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Weber.VFAO.MultiTenancy.Dto;
using Weber.VFAO.MultiTenancy.Payments.Dto;

namespace Weber.VFAO.MultiTenancy.Payments
{
    public interface IPaymentAppService : IApplicationService
    {
        Task<PaymentInfoDto> GetPaymentInfo(PaymentInfoInput input);

        Task<CreatePaymentResponse> CreatePayment(CreatePaymentDto input);

        Task<ExecutePaymentResponse> ExecutePayment(ExecutePaymentDto input);
    }
}
