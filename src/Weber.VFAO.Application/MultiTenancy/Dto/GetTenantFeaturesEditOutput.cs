using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Weber.VFAO.Editions.Dto;

namespace Weber.VFAO.MultiTenancy.Dto
{
    public class GetTenantFeaturesEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}