using System.Threading.Tasks;
using Abp.Application.Services;
using Weber.VFAO.Editions.Dto;
using Weber.VFAO.MultiTenancy.Dto;

namespace Weber.VFAO.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}