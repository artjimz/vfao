﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Weber.VFAO.Authorization.Permissions.Dto;

namespace Weber.VFAO.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
