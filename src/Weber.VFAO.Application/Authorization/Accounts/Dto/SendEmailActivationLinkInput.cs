﻿using System.ComponentModel.DataAnnotations;

namespace Weber.VFAO.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}