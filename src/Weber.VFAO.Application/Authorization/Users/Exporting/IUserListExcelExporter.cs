using System.Collections.Generic;
using Weber.VFAO.Authorization.Users.Dto;
using Weber.VFAO.Dto;

namespace Weber.VFAO.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}