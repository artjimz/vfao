﻿using Weber.VFAO.Security;

namespace Weber.VFAO.Authorization.Users.Profile.Dto
{
    public class GetPasswordComplexitySettingOutput
    {
        public PasswordComplexitySetting Setting { get; set; }
    }
}
