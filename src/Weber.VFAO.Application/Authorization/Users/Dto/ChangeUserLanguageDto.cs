﻿using System.ComponentModel.DataAnnotations;

namespace Weber.VFAO.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
