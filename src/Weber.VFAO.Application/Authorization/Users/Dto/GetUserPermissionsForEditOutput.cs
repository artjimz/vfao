﻿using System.Collections.Generic;
using Weber.VFAO.Authorization.Permissions.Dto;

namespace Weber.VFAO.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}