﻿using Abp.Notifications;
using Weber.VFAO.Dto;

namespace Weber.VFAO.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}