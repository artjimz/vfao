﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Weber.VFAO.Configuration.Host.Dto;

namespace Weber.VFAO.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
