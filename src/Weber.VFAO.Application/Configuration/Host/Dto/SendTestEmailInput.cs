﻿using System.ComponentModel.DataAnnotations;
using Weber.VFAO.Authorization.Users;

namespace Weber.VFAO.Configuration.Host.Dto
{
    public class SendTestEmailInput
    {
        [Required]
        [MaxLength(User.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}