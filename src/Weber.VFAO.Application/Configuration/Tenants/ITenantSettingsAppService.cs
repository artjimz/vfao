﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Weber.VFAO.Configuration.Tenants.Dto;

namespace Weber.VFAO.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
