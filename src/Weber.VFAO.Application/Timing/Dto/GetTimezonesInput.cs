﻿using Abp.Configuration;

namespace Weber.VFAO.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope { get; set; }
    }
}
