﻿using Weber.VFAO.Dto;

namespace Weber.VFAO.Common.Dto
{
    public class FindUsersInput : PagedAndFilteredInputDto
    {
        public int? TenantId { get; set; }
    }
}