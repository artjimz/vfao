using System.Threading.Tasks;

namespace Weber.VFAO.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task ValidateAsync(string captchaResponse);
    }
}