﻿using System.Collections.Generic;

namespace Weber.VFAO.Web.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}