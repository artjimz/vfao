﻿using Abp.AutoMapper;
using Weber.VFAO.Web.Authentication.External;

namespace Weber.VFAO.Web.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
