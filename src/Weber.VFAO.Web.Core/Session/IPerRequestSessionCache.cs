﻿using System.Threading.Tasks;
using Weber.VFAO.Sessions.Dto;

namespace Weber.VFAO.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
