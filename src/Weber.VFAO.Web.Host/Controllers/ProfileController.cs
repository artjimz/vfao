﻿using Abp.AspNetCore.Mvc.Authorization;

namespace Weber.VFAO.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ProfileController : ProfileControllerBase
    {
        public ProfileController(IAppFolders appFolders)
            : base(appFolders)
        {
        }
    }
}