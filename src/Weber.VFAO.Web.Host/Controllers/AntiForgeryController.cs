﻿using Microsoft.AspNetCore.Antiforgery;

namespace Weber.VFAO.Web.Controllers
{
    public class AntiForgeryController : VFAOControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
