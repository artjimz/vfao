﻿using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;

namespace Weber.VFAO.Web.Controllers
{
    public class HomeController : VFAOControllerBase
    {
        [DisableAuditing]
        public IActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}
