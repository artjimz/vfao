﻿using System.Threading.Tasks;
using Abp;
using Abp.Notifications;
using Weber.VFAO.Authorization.Users;
using Weber.VFAO.MultiTenancy;

namespace Weber.VFAO.Notifications
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

        Task NewUserRegisteredAsync(User user);

        Task NewTenantRegisteredAsync(Tenant tenant);

        Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info);
    }
}
