﻿using System.Threading.Tasks;

namespace Weber.VFAO.Security
{
    public interface IPasswordComplexitySettingStore
    {
        Task<PasswordComplexitySetting> GetSettingsAsync();
    }
}
