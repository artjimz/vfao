﻿namespace Weber.VFAO.Chat
{
    public enum ChatSide
    {
        Sender = 1,

        Receiver = 2
    }
}