﻿using System.Threading.Tasks;
using Abp.Domain.Policies;

namespace Weber.VFAO.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}
