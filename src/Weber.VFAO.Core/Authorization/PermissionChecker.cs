﻿using Abp.Authorization;
using Weber.VFAO.Authorization.Roles;
using Weber.VFAO.Authorization.Users;

namespace Weber.VFAO.Authorization
{
    /// <summary>
    /// Implements <see cref="PermissionChecker"/>.
    /// </summary>
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
