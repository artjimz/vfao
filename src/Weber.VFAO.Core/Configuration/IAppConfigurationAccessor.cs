﻿using Microsoft.Extensions.Configuration;

namespace Weber.VFAO.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
