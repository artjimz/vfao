﻿namespace Weber.VFAO
{
    public class VFAOConsts
    {
        public const string LocalizationSourceName = "VFAO";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const int PaymentCacheDurationInMinutes = 30;
    }
}