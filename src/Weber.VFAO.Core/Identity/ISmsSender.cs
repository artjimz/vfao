using System.Threading.Tasks;

namespace Weber.VFAO.Identity
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}