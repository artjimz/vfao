﻿using Abp.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;
using Weber.VFAO.Authorization.Roles;
using Weber.VFAO.Authorization.Users;
using Weber.VFAO.MultiTenancy;

namespace Weber.VFAO.Identity
{
    public class SecurityStampValidator : AbpSecurityStampValidator<Tenant, Role, User>
    {
        public SecurityStampValidator(
            IOptions<IdentityOptions> options, 
            SignInManager signInManager) 
            : base(options, signInManager)
        {
        }
    }
}