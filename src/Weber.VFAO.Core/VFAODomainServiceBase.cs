﻿using Abp.Domain.Services;

namespace Weber.VFAO
{
    public abstract class VFAODomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected VFAODomainServiceBase()
        {
            LocalizationSourceName = VFAOConsts.LocalizationSourceName;
        }
    }
}
