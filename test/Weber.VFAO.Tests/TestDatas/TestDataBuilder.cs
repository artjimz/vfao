﻿using Weber.VFAO.EntityFrameworkCore;

namespace Weber.VFAO.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly VFAODbContext _context;
        private readonly int _tenantId;

        public TestDataBuilder(VFAODbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            new TestOrganizationUnitsBuilder(_context, _tenantId).Create();

            _context.SaveChanges();
        }
    }
}
