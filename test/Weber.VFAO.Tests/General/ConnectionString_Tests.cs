﻿using System.Data.SqlClient;
using Shouldly;
using Xunit;

namespace Weber.VFAO.Tests.General
{
    public class ConnectionString_Tests
    {
        [Fact]
        public void SqlConnectionStringBuilder_Test()
        {
            var csb = new SqlConnectionStringBuilder("Server=localhost; Database=VFAO; Trusted_Connection=True;");
            csb["Database"].ShouldBe("VFAO");
        }
    }
}
