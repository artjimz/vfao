﻿using System.Threading.Tasks;
using Weber.VFAO.Security.Recaptcha;

namespace Weber.VFAO.Tests.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public async Task ValidateAsync(string captchaResponse)
        {
            
        }
    }
}
